#ifndef _BS8116_H
#define _BS8116_H

#include "stm32f4xx.h"

//宏定义
//时钟线宏定义
#define BS8116_IIC_SCL_L (GPIO_ResetBits(GPIOB, GPIO_Pin_6))
#define BS8116_IIC_SCL_H (GPIO_SetBits(GPIOB, GPIO_Pin_6))

//数据线宏定义
//输出
#define BS8116_IIC_SDA_L (GPIO_ResetBits(GPIOB, GPIO_Pin_7))
#define BS8116_IIC_SDA_H (GPIO_SetBits(GPIOB, GPIO_Pin_7))

//输入
#define BS8116_IIC_SDA_IN (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_7))
#define BS8116_IIC_SCL_IN (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_6))

//读写器件地址
#define BS8116_W (0xa0)
#define BS8116_R (0xa1)

//IRQ --- 检测按键是否按下
#define BS8116_IRQ (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_1))

//声明
void BS8116_init(void);
u8 BS8116_ReadKey(void);
u8 BS8116_Key_scan(void);
void open_passward(u8 bs8116_key);
#endif

