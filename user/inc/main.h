#ifndef _MAIN_H
#define _MAIN_H

#include "stm32f4xx.h"
extern u8 page_flag;

#include "stm32f4xx.h"                  // Device header
#include "led.h"
#include "key.h"
#include "usart.h"
#include "stdio.h"
#include "nvic.h"
#include "motor.h"
#include "time.h"
#include "voice.h"
#include "lcd.h"
#include "font.h"
#include "pic.h"
#include "bs8116.h"
#include "at24c02.h"
#include "mg200.h"
#include "string.h"
#include "function.h"
#include "rfid.h"

#endif

