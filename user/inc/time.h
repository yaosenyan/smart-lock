#ifndef _TIME_H
#define _TIME_H

#include "stm32f4xx.h"

void Tim5_Delay_Ms(u32 ms);
void Tim5_Delay_Us(u32 us);
#endif


