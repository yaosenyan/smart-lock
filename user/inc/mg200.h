#ifndef _MG200_H
#define _MG200_H

#include "stm32f4xx.h"

//�궨��
#define MG200_DETECT  (GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_0))	 
#define MG200_PWR_ON  (GPIO_SetBits(GPIOC,GPIO_Pin_1))
#define MG200_PWR_OFF (GPIO_ResetBits(GPIOC,GPIO_Pin_1))

void mg200_init(void);
u8 ReqCaptureAndExtract(u8 parameter);
u8 Enroll(u8 ID);
u8 Match_l_n(void);
u8 erase_all(void);
u8 erase_one(u8 id);

#endif

