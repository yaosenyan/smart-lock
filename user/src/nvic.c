#include "nvic.h"
#include "string.h"
#include "led.h"
#include "motor.h"
#include "voice.h"
/********************************************************************
* 函数名 	：USART1_IRQHandler 
* 函数功能	：串口1中断服务函数
* 函数参数	：void
* 函数返回值：void
* 函数说明	：
*********************************************************************/
u8 usart1_buff[30];
void USART1_IRQHandler(void)
{
	static u8 i = 0;
	//接收中断信号触发
	if(USART_GetITStatus(USART1, USART_IT_RXNE))         //判断如果是接收中断
	{
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);  //清除中断标志
		
		usart1_buff[i++] = USART_ReceiveData(USART1);  //接收数据
		
	}
	//空闲中断信号触发
	if(USART_GetITStatus(USART1, USART_IT_IDLE))  
	{
		//清除空闲中断标志位
		USART1->SR;  			//读状态寄存器
		USART1->DR;  			//读数据寄存器
		//数据处理
		usart1_buff[i] = '\0';
		i = 0;
		
		//判断数据执行操作
		if(strncmp((char *)usart1_buff,"欢迎回家",4) == 0)
		{
			Voice(0X18);
			LED3_ON;
			LED4_ON;
		}
		else if(strncmp((char *)usart1_buff,"登记指纹",4) == 0)
		{
			Voice(0X03);
			LED3_OFF;
			LED4_OFF;
		}
		else if(strncmp((char *)usart1_buff,"修改时间",4) == 0)
		{
			Voice(0X05);
			LED3_ON;
			LED4_ON;
		}
		else if(strncmp((char *)usart1_buff,"登记卡片",4) == 0)
		{
			Voice(0X04);
			LED3_OFF;
			LED4_OFF;
		}
	}
}

/******************************************
*函数名    ：USART6_IRQHandler                 
*函数功能  ：发送一个字符中断函数
*函数参数  ：无
*函数返回值：无
*函数描述  ：无
*********************************************/
u8 mg200_rec_buff[10];
u8 mg200_rec_flag = 0;
void USART6_IRQHandler(void)
{
	static u8 i = 0;
	//接收中断信号触发
	if(USART_GetITStatus(USART6, USART_IT_RXNE))         //判断如果是接收中断
	{
		USART_ClearITPendingBit(USART6, USART_IT_RXNE);  //清除中断标志
		
		mg200_rec_buff[i++] = USART_ReceiveData(USART6);  //接收数据
		
	}
	//空闲中断信号触发
	if(USART_GetITStatus(USART6, USART_IT_IDLE))  
	{
		//清除空闲中断标志位
		USART6->SR;  			//读状态寄存器
		USART6->DR;  			//读数据寄存器
		i = 0;
		mg200_rec_flag = 1;
	}
}


