#include "at24c02.h"
#include "time.h"

/***********************************************
*函数名    :iic_IO_init
*函数功能  :IIC所用IO口初始化配置
*函数参数  :无
*函数返回值:无
*函数描述  :PA8----SCL   通用推挽输出
            PC9----SDA   通用开漏输出
************************************************/
void iic_IO_init(void)
{
	
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;				//PA8（选择8号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOA, &GPIO_InitStruct); 
	//PC9
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;				//PA8（选择8号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	//空闲状态
	IIC_SCL_H;
	IIC_SDA_OUT_H;
}


/***********************************************
*函数名    :iic_star
*函数功能  :IIC的起始信号函数
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void iic_star(void)
{
	//拉低时钟线      //为了可以改变数据线
	IIC_SCL_L;
	
  //拉高数据线      //为了产生下降沿
	IIC_SDA_OUT_H;
	
	//拉高钟据线      //产生起始信号的条件
	IIC_SCL_H;
	
	//延时5us
	Tim5_Delay_Ms(5);
	
	//拉低数据线      //产生下降沿从而产生起始信号
	IIC_SDA_OUT_L;
	
	//延时5us
	Tim5_Delay_Ms(5);
	
	//拉低时钟线      //位了避免时钟在高电平状态下，数据线改变而出现起始信号或者停止信号
	IIC_SCL_L;
	
}


/***********************************************
*函数名    :iic_stop
*函数功能  :IIC的停止信号函数
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void iic_stop(void)
{
	//拉低时钟线
	IIC_SCL_L;       //为了可以动数据线
	
	//拉低数据线
	IIC_SDA_OUT_L;   //为了可以产生上升沿
	
	//拉高时钟线
	IIC_SCL_H;       //产生停止信号条件
	
	//延时5us
	Tim5_Delay_Us(5);
	
	//拉高数据线
	IIC_SDA_OUT_H;   //产生上升沿从而产生停止信号
	
	//延时5us
	Tim5_Delay_Us(5);
	
}


/***********************************************
*函数名    :iic_send_ack
*函数功能  :IIC发送应答/不应答函数
*函数参数  :u8 ack
*函数返回值:无
*函数描述  :ack:  0   应答
           ack:  1   不应答
************************************************/
void iic_send_ack(u8 ack)
{
	IIC_SCL_L;     //为了可以动数据线
	//延时5us
	Tim5_Delay_Us(5);
	if(ack==0)
	{
		IIC_SDA_OUT_L;    //为了可以产生应答信号
	}
	else
	{
		IIC_SDA_OUT_H;    //为了可以产生不应答信号
	}
	
	//延时5us
	Tim5_Delay_Us(5);
	
	IIC_SCL_H;          //为了产生应答信号/不应答信号
	
	//延时5us
	Tim5_Delay_Us(5);
  	
	//安全作用
	IIC_SCL_L; 
}

/***********************************************
*函数名    :iic_get_ack
*函数功能  :IIC返回应答/不应答函数
*函数参数  :无
*函数返回值:u8
*函数描述  :返回0   应答
           返回1   不应答
************************************************/
u8 iic_get_ack(void)
{
	u8 ack;
	/*把数据线切换为输入模式*/
	IIC_SCL_L;        //为了可以动数据线
	IIC_SDA_OUT_H;    //把输出路堵死
	
	/*检测应答*/
	IIC_SCL_L;        //

	//延时5us
	Tim5_Delay_Us(5);
	
	IIC_SCL_H;        //
	
	//延时5us
	Tim5_Delay_Us(5);
	
	if(IIC_SDA_INT)    //
	{
		ack = 1;
	}
	else
	{
		ack = 0;
	}
	
	//安全作用
	IIC_SCL_L;
	
	return ack;
	
}

/***********************************************
*函数名    :iic_send_byte
*函数功能  :IIC发送一个字节函数
*函数参数  :u8
*函数返回值:无
*函数描述  :
************************************************/
void iic_send_byte(u8 data)
{
	u8 i;
	for(i=0;i<8;i++)     //循8次
	{
		IIC_SCL_L;         //为了写入数据
		//延时5us
	  Tim5_Delay_Us(5);
		if(data & 0x80)    //判断最高位是什么
		{
		  IIC_SDA_OUT_H;
		}
		else
		{
			IIC_SDA_OUT_L;
		}
		//延时5us
	  Tim5_Delay_Us(5);
		IIC_SCL_H;          //帮助从机读取数据线的数据
		//延时5us
	  Tim5_Delay_Us(5);
		data = data<<1;     //把此高位变成最高位
	}
	
	//安全作用
	IIC_SCL_L;
	
	
}

/***********************************************
*函数名    :iic_rec_byte
*函数功能  :IIC接收一个字节函数
*函数参数  :无
*函数返回值:u8 
*函数描述  :
************************************************/
u8 iic_read_byte(void)
{
	u8 i;
	u8 data;
	/*将输出线切换为输入模式*/
	IIC_SCL_L;
  IIC_SDA_OUT_H;
	
	
	/*接收一个字节数据*/
	//帮助从机发送数据
	
	for(i=0;i<8;i++)
	{
		IIC_SCL_L;
		//延时5us
	  Tim5_Delay_Us(5);
	 	IIC_SCL_H;
		//延时5us
  	Tim5_Delay_Us(5);
		data = data << 1;
		if(IIC_SDA_INT)
		{
			data = data | 0x01;
		}
  }
	//安全
	IIC_SCL_L;
	
	
	return data;
}


/***********************************************
*函数名    :at24c02_init
*函数功能  :at24c02初始化
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void at24c04_init(void)
{
	iic_IO_init();
}




/***********************************************
*函数名    :at24c04_write_byte
*函数功能  :往at24c04某个空间存储一个字节数据
*函数参数  :u16 inner_addr,      //器件内部存储空间地址  0~511 
            u8 data              //要写入的数据
*函数返回值:u8 
*函数描述  :T24C02_NO_ERR  0;   //应答  无错误
						AT24C02_ERR1   1;   //无应答
						AT24C02_ERR2   2;   //无应答
						AT24C02_ERR3   3;   //无应答
************************************************/
u8 AT24C04_write_byte(u16 inner_addr,u8 data)
{
	u8 ack;
	//开始信号函数
	iic_star();
	
	//发送器件地址函数
	if(inner_addr<=255)                     //存到第一块
	{
		iic_send_byte(AT24C04_ADDR0_W);       //第一块的写器件地址
	}
	else                                    //存到第二块
	{
		iic_send_byte(AT24C04_ADDR1_W);       //第二块的写器件地址
	}
	//接收应答函数
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR1;
	}
	
	//发送内部地址函数
	iic_send_byte(inner_addr & 0xff);
	
	//接收应答函数
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR2;
	}
	
	//发送数据函数
	iic_send_byte(data);
	
	//接收应答函数
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR3;
	}
	
	//停止信号函数
	iic_stop();
	
	//内部写周期
	Tim5_Delay_Ms(5);
	
	//返回值
	return AT24C04_NO_ERR;
}


/********************************************************
*函数名    :AT24C04_read_byte
*函数功能  :从AT24C04的某个地址空间读一个字节数据
*函数参数  :u16 inner_addr     //器件内部存储空间地址  0~511   
						u8 *data          //读到的数据存放的地址
*函数返回值:每次发送都会检测响应，通过返回值判断具体哪次发送出问题			
*函数描述  :宏定义错误标志：
							AT24C04_NO_ERR 0;   //应答  无错误
							AT24C04_ERR1   1;   //无应答
							AT24C04_ERR2   2;   //无应答
							AT24C04_ERR3   3;   //无应答
**********************************************************/
u8 AT24C04_read_byte(u16 inner_addr,u8 *data)
{
	u8 ack;
	
	iic_star();
	if(inner_addr<=255)   //存到第一块
	{
		iic_send_byte(AT24C04_ADDR0_W);    //第一块的写器件地址
	}
	else                  //存第二块
	{
		iic_send_byte(AT24C04_ADDR1_W);    //第二块的写器件地址
	} 
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR1; 
	}
	
	/*发送绝对地址*/
	iic_send_byte(inner_addr&0xff);
	//inner_addr = inner_addr & 0xff;    //第二块的实际绝对地址
	//inner_addr = inner_addr % 256;    //第二块的实际绝对地址
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR2; 
	}
	
	
	iic_star();
	if(inner_addr<=255)   //读第一块
	{
		iic_send_byte(AT24C04_ADDR0_R);    //第一块的读器件地址
	}
	else                  //读第二块
	{
		iic_send_byte(AT24C04_ADDR1_R);    //第二块的读器件地址   
	} 
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR1; 
	}	
	
	*data = iic_read_byte();
	iic_send_ack(1);
	iic_stop();
	
	return AT24C04_NO_ERR;
	
}

/********************************************************
*函数名    :AT24C04_read_bytes
*函数功能  :从AT24C04的某个地址空间开始连续读多个字节数据
*函数参数  :u16 inner_addr     //器件内部存储空间地址  0~511  
            u8 num            //要读多少个字节
						u8 *str           //读到的数据存放的地址
*函数返回值:每次发送都会检测响应，通过返回值判断具体哪次发送出问题			
*函数描述  :宏定义错误标志：
							AT24C04_NO_ERR 0;   //应答  无错误
							AT24C04_ERR1   1;   //无应答
							AT24C04_ERR2   2;   //无应答
							AT24C04_ERR3   3;   //无应答
**********************************************************/
u8 AT24C04_read_bytes(u16 inner_addr,u16 num,u8 *str)
{
	u8 ack;
	
	iic_star();
	if(inner_addr<=255)   //存到第一块
	{
		iic_send_byte(AT24C04_ADDR0_W);    //第一块的写器件地址
		
	}
	else                  //存第二块
	{
		iic_send_byte(AT24C04_ADDR1_W);    //第二块的写器件地址
	} 
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR1; 
	}	
	
	/*发送绝对地址*/
	iic_send_byte(inner_addr&0xff);
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR2; 
	}
	
	iic_star();
	if(inner_addr<=255)   //读第一块
	{
		iic_send_byte(AT24C04_ADDR0_R);    //第一块的读器件地址
	}
	else                  //读第二块
	{
		iic_send_byte(AT24C04_ADDR1_R);    //第二块的读器件地址   
	} 
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR1; 
	}	
	
	//循环读数据
	while(1)
	{
		*str = iic_read_byte();
		num--;
		if(num == 0)
		{
			iic_send_ack(1);
			break;
		}
		iic_send_ack(0);
		str++;
	}
	
	iic_stop();
	
	return AT24C04_NO_ERR;
}


/********************************************************
*函数名    :AT24C04_write_page
*函数功能  :往AT24C04的某个地址空间开始页写
*函数参数  :u16 inner_addr     //器件内部存储空间地址  0~511  
            u8 num            //要读多少个字节
						u8 *str           //要写的字符串的首地址
						AT_flag            02代表AT24C02    04代表AT24C04
*函数返回值:每次发送都会检测响应，通过返回值判断具体哪次发送出问题			
*函数描述  :宏定义错误标志：
							AT24C04_NO_ERR 0;   //应答  无错误
							AT24C04_ERR1   1;   //无应答
							AT24C04_ERR2   2;   //无应答
							AT24C04_ERR3   3;   //无应答
              AT24C04_ERR4   4;   //跨页错误
**********************************************************/
u8 AT24C04_write_page(u16 inner_addr,u8 num,u8 *str,u8 AT_flag)
{
	u8 ack;
	
	/*判断是哪个型号的存储芯片*/
	if(AT_flag == AT24C02)
	{
			/*判断要写的内容是否会跨页*/
		if(inner_addr / 8 != (inner_addr + num - 1) / 8)     //AT24C02芯片，每页8字节
		{
			return AT24C04_ERR4;
		}
	
	}
	else if(AT_flag == AT24C04)
	{
		/*判断要写的内容是否会跨页*/
		if(inner_addr / 16 != (inner_addr + num - 1) / 16)     //AT24C04芯片，每页16字节
		{
			return AT24C04_ERR4;
		}
	}
	

	
	
	
	
	/*页写*/
	iic_star();
	if(inner_addr<=255)   //存到第一块
	{
		iic_send_byte(AT24C04_ADDR0_W);    //第一块的写器件地址
		
	}
	else                  //存第二块
	{
		iic_send_byte(AT24C04_ADDR1_W);    //第二块的写器件地址
	} 
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR1; 
	}	
	
	/*发送绝对地址*/
	iic_send_byte(inner_addr&0xff);
	ack = iic_get_ack();
	if(ack == 1)
	{
		iic_stop();
		return AT24C04_ERR2; 
	}
	
	while(1)
	{
		iic_send_byte(*str);
		ack = iic_get_ack();
		if(ack == 1)
		{
			iic_stop();
			return AT24C04_ERR3; 
		}	
		num--;
		if(num == 0)
		{
			break;
		}
		str++;
	}
	iic_stop();
	
	//写周期
	Tim5_Delay_Ms(5);
	
	return AT24C04_NO_ERR;
}


/********************************************************
*函数名    :AT24C04_write_bytes
*函数功能  :往AT24C04的某个地址空间开始连续写可以跨页
*函数参数  :u16 inner_addr     //器件内部存储空间地址  0~511  
            u16 num            //要读多少个字节
						u8 *str           //要写的字符串的首地址
*函数返回值:每次发送都会检测响应，通过返回值判断具体哪次发送出问题			
*函数描述  :宏定义错误标志：
							AT24C04_NO_ERR 0;   //应答  无错误
							AT24C04_ERR1   1;   //无应答
							AT24C04_ERR2   2;   //无应答
							AT24C04_ERR3   3;   //无应答
**********************************************************/
u8 AT24C04_write_bytes(u16 inner_addr,u16 num,u8 *str,u8 AT_flag)
{
	u8 less_num;
	
	while(1)
	{
		/*判断是哪个型号的存储芯片*/
		if(AT_flag == AT24C02)
		{
			//计算本页还剩多少个空间可以写
			less_num = 8 - inner_addr % 8;             //AT24C02芯片，每页8字节
		}
		else if(AT_flag == AT24C04) 
		{
			//计算本页还剩多少个空间可以写
			less_num = 16 - inner_addr % 16;             //AT24C04芯片，每页16字节 
		}
		
		
		//判断是否会跨页
		if(less_num >= num)       //不跨页
		{
			//调用页写函数将写完
			AT24C04_write_page(inner_addr,num,str,AT_flag);  
			//退出循环结构
			break;
		}
		//跨页
		else
		{
			//先调用页写函数把本页写完
			AT24C04_write_page(inner_addr,less_num,str,AT_flag);  // 
			//存储芯片切换到下页的起始位置位置
			inner_addr = inner_addr + less_num;   //253+3==256
			
			//计算出剩余要写的个数
			num = num - less_num;                 //7
			
			//计算出剩余要写的内容的地址
			str = str + less_num;	
		}
	}		
	return AT24C04_NO_ERR;
}








