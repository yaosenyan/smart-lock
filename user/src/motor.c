#include "motor.h"
#include "time.h"
/********************************************************************
* 函数名 	：Motor_Init
* 函数功能	：门锁电机初始化函数（门锁连接的GPIO口初始化配置函数）
* 函数参数	：void
* 函数返回值：void
* 函数说明	：
*			 MOTOR_IA --- PB4 --- 通用推挽输出，速度2MHz，无上下拉
*      MOTOR_IB --- PB3 --- 通用推挽输出，速度2MHz，无上下拉
*********************************************************************/
void Motor_Init(void)
{
	//打开GPIO控制时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	//把PB4和PB3配置为通用推挽输出，速度2MHz,无上下拉
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_4;		//PB4、PB3（选择4、3号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 
	
	//关闭电机(PB3和PB4输出低电平)
	 Motor_Close();
	
}

/********************************************************************
* 函数名 	：Motor_Open
* 函数功能	：开锁函数
* 函数参数	：void
* 函数返回值：void
* 函数说明	：
*			 MOTOR_IA输出高电平，MOTOR_IB输出低电平
*********************************************************************/
void Motor_Open(void)
{
	//电机转动
			GPIO_SetBits(GPIOB, GPIO_Pin_4);
		  GPIO_ResetBits(GPIOB, GPIO_Pin_3);
	//延时300ms
	Tim5_Delay_Ms(300);
	//停止转动
			GPIO_SetBits(GPIOB, GPIO_Pin_4);
		  GPIO_SetBits(GPIOB, GPIO_Pin_3);
}

/********************************************************************
* 函数名 	：Motor_Close
* 函数功能	：关锁函数
* 函数参数	：void
* 函数返回值：void
* 函数说明	：
*			 MOTOR_IA输出低电平，MOTOR_IB输出高电平
*********************************************************************/
void Motor_Close(void)
{
	//电机转动
			GPIO_SetBits(GPIOB, GPIO_Pin_3);
		  GPIO_ResetBits(GPIOB, GPIO_Pin_4);
	//延时300ms
	Tim5_Delay_Ms(300);
	//停止转动
			GPIO_SetBits(GPIOB, GPIO_Pin_3);
		  GPIO_SetBits(GPIOB, GPIO_Pin_4);
}


