#include "time.h"

/********************************************************************
* 函数名 	：Tim5_Delay_Ms
* 函数功能	：定时器5的毫秒级延时函数
* 函数参数	：u32 ms  延时时间
* 函数返回值：void
* 函数说明	：
*			  APB1  84M  8400  10次/ms
*********************************************************************/
void Tim5_Delay_Ms(u32 ms)
{
	//打开定时器5的时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
	//选择单脉冲模式
	TIM_SelectOnePulseMode(TIM5,TIM_OPMode_Single);
	//定时器配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};
	TIM_TimeBaseInitStruct.TIM_ClockDivision =  TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;   //计数模式---选择向上计数
	TIM_TimeBaseInitStruct.TIM_Period = ms*10 -1;				//重载值
	TIM_TimeBaseInitStruct.TIM_Prescaler = 8400 - 1;		//预分频
	TIM_TimeBaseInit(TIM5, &TIM_TimeBaseInitStruct);
	//计数器清零
	TIM_ClearFlag(TIM5, TIM_FLAG_Update);
	//开始计数
	TIM_Cmd(TIM5,ENABLE);
	//等待计数完成
	while(!TIM_GetFlagStatus(TIM5,TIM_FLAG_Update));
}

/********************************************************************
* 函数名 	：Tim5_Delay_Us
* 函数功能	：定时器5的微秒级延时函数
* 函数参数	：u32 us  延时时间
* 函数返回值：void
* 函数说明	：
*			  APB1  84M  42  2次/us
*********************************************************************/
void Tim5_Delay_Us(u32 us)
{
	//打开定时器5的时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
	//定时器配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_Period = us*2 -1;				//重载值
	TIM_TimeBaseInitStruct.TIM_Prescaler = 42 - 1;		//预分频

	TIM_TimeBaseInit(TIM5, &TIM_TimeBaseInitStruct);
	//选择单脉冲模式
	TIM_SelectOnePulseMode(TIM5,TIM_OPMode_Single);
	//计数器清零
	TIM_ClearFlag(TIM5, TIM_FLAG_Update);
	//开始计数
	TIM_Cmd(TIM5,ENABLE);
	//等待计数完成
	while(!TIM_GetFlagStatus(TIM5,TIM_FLAG_Update));
}





