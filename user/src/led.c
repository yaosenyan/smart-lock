#include "led.h"

/********************************************************************
* 函数名 	：Led_Init
* 函数功能	：led灯初始化函数（对led灯所连接的GPIO口初始化配置）
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  LED3 -- PB9 ---通用推挽输出，速度2MHz，不用上下拉
*			  LED4 -- PB8 ---通用推挽输出，速度2MHz，不用上下拉
*********************************************************************/
void Led_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;				//PB9（选择9号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;				//PB9（选择8号GPIO口）
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	
	//关闭LED灯（）
	LED3_OFF;
	LED4_OFF;
}

