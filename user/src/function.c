#include "main.h"

/***********************************************
*函数名    :admin_page
*函数功能  :管理员界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 2
************************************************/
void admin_page(u8 key)
{
	static u8 ad_flag = 1;
	
	//执行一次
	if(ad_flag)
	{
		printf("管理员界面：\r\n");
		printf("【1】修改管理员密码\r\n");
		printf("【2】指纹管理\r\n");
		printf("【3】射频卡管理\r\n");
		printf("【4】声音调节\r\n");
		printf("【5】屏幕亮度调节\r\n");         //定时器输出PWM
		printf("【#】退出管理员界面\r\n");
		
		ad_flag = 0;
	}
	

	/*判断按键的值进入相应的功能*/
	
	//退出管理员界面
	if(key == '#')
	{
		page_flag = 1;      //回到主界面
		ad_flag = 1;        //下次进来显示界面
		
	}
	
	//密码管理界面
	else if(key == '1')
	{
		page_flag = 6;      //进入修改管理员密码界面
		ad_flag = 1;        //下次进来显示界面
	}
	
	//指纹管理界面
	if(key == '2')
	{
		page_flag = 3;  //进入指纹管理界面
		ad_flag = 1;    //下次进来显示界面
		
	}
	
	//射频卡管理界面
	if(key == '3')
	{
		page_flag = 10;  //进入指纹管理界面
		ad_flag = 1;    //下次进来显示界面
		
	}
	
	//音量管理界面
	if(key == '4')
	{
		page_flag = 7;  //进入音量管理界面
		ad_flag = 1;    //下次进来显示界面
		
	}
	//屏幕亮度调节
	else if(key == '5')
	{
		page_flag = 8;  //进入屏幕亮度调节界面  
		ad_flag = 1;    //下次进来显示界面
	}
}


/***********************************************
*函数名    :mg200_page
*函数功能  :指纹管理界面
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 3
************************************************/
u8 mg200_id[9] = {0};

void mg200_page(u8 key)
{
	static u8 mg_flag = 1;
	u8 mg200_open_flag;
	
	if(mg_flag)
	{
		printf("指纹管理界面：\r\n");
		printf("【1】注册指纹\r\n");
		printf("【2】删除指定指纹\r\n");
		printf("【3】删除所有指纹\r\n");
		printf("【#】回到上一级菜单\r\n");
		printf("【*】回到主界面\r\n");
		mg_flag = 0;
		AT24C04_read_byte(20,&mg200_open_flag);
		printf("mg200_open_flag:0x%x\r\n",mg200_open_flag);
	}
	
	//检测是为第一次开机
//	AT24C04_read_byte(20,&mg200_open_flag);
	if(mg200_open_flag != 0xfe)
	{
		AT24C04_write_bytes(21,sizeof(mg200_id),mg200_id,AT24C02);   //将清空后的数组写入AT24中
		AT24C04_write_byte(20,0xfe);                         //证明开机过
		//printf("111\r\n");	
		//erase_all();                                         //删除所有指纹 
	}
	
	
	//注册指纹
	if(key == '1')
	{
		page_flag = 5; //注册指纹界面标志位
		mg_flag = 1;   //下次进来显示界面
		
	}
	
	
	//指定指纹删除
	if(key == '2')
	{
		page_flag = 4; //删除指定指纹界面标志位
		mg_flag = 1;   //下次进来显示界面
	}
	
	
	//删除所有指纹
	if(key == '3')
	{
		if(erase_all() == 0) 
		{
			//AT24C02存储指纹的地方清空
			u8 ee[9] = {0};
			AT24C04_write_bytes(21,sizeof(ee),ee,AT24C02);
		}			
	}

	
	//返回上一级
	if(key == '#')
	{
		page_flag = 2; //管理员界面标志位
		mg_flag = 1;   //下次进来显示界面
	} 
	
	
	//返回主界面
	if(key == '*')
	{
		page_flag = 1;  //主界面标志位
		mg_flag = 1;   //下次进来显示界面
	}

	
}



/***********************************************
*函数名    :Enroll_page
*函数功能  :注册指纹界面函数
*函数参数  :
*函数返回值:无
*函数描述  :page_flag = 5
************************************************/
void Enroll_page(void)
{
	
	u8 mg200_i = 0;
	u8 mg200_ID;
	u8 mg200_Enroll_flag;
	
	//将AT24原有指纹id读出来放在数组中
	AT24C04_read_bytes(21,sizeof(mg200_id),mg200_id);
	
	//测试打印
	for(u8 i=0;i<9;i++)
	{
		if(mg200_id[i] != 0)
		{
			printf("\r\nID:%d\r\n",mg200_id[i]);
		}
	}

	//通过遍历数组元素0的下标+1,就是可以注册的ID
	while(mg200_id[mg200_i] != 0)
	{
		mg200_i++;
	}
	//出来循环下标就是要写入的位置,下标+1就是ID号
	mg200_ID = mg200_i+1;
	//注册
	mg200_Enroll_flag = Enroll(mg200_ID);
	if(mg200_Enroll_flag == 0)       //如果注册成功，更新
	{
		//将注册到的ID写到数组中
		mg200_id[mg200_i] = mg200_ID;
		//将数组写到AT24c02中
		AT24C04_write_bytes(21,sizeof(mg200_id),mg200_id,AT24C02);
		
	}
	
	//自动回到指纹管理界面
	page_flag = 3;
	
	
}


/***********************************************
*函数名    :erase_one_page
*函数功能  :删除指定ID指纹界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 4
************************************************/
void erase_one_page(u8 key)
{
	static u8 erase_flag = 1;
	if(erase_flag)
	{
		erase_flag = 0;
		
		printf("删除指定指纹界面：\r\n");
		printf("【#】回到上一级菜单\r\n");
		printf("【*】回到主界面\r\n");
		printf("请选择要删除的ID号:\r\n");
		
		//从AT24中读出有效可删除ID号显示
		AT24C04_read_bytes(21,sizeof(mg200_id),mg200_id);
		for(u8 i=0;i<9;i++)
		{
			if(mg200_id[i] != 0)
			{
				printf("指纹ID:%d\r\n",mg200_id[i]);
			}
		}
		
	}
	
	//删除
	
	if(key != 0xff && key != '#' && key != '*' && key != '0')
	{
		u8 erase_sta;
		//对指纹模块指定指纹进行删除
		erase_sta = erase_one(key-48);
		if(erase_sta == 0)
		{
			//数组指定位置的ID变为0
			mg200_id[key-48-1] = 0;
			//更新到AT24中
			AT24C04_write_bytes(21,sizeof(mg200_id),mg200_id,AT24C02);
		}

	}

	
	//返回上一级
	if(key == '#')
	{
		page_flag = 3;    //指纹管理界面标志位
		erase_flag = 1;   //下次进来显示界面
	} 
	//返回主界面
	if(key == '*')
	{
		page_flag = 1;  //主界面标志位
		erase_flag = 1;   //下次进来显示界面
	}

}





/***********************************************
*函数名    :change_password
*函数功能  :修改密码界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 6
************************************************/
void change_password(u8 key)
{
	static u8 sys_password[7];	//存第一次密码
	static u8 user_password[7];	//存第二次密码
	static u8 pas_flag = 1;
	static u8 pass_i = 0;
	
	if(pas_flag)
	{
		printf("请输入你要修改的密码:\r\n");
		printf("【#】回到上一级菜单\r\n");
		printf("【*】回到主界面\r\n");
		Voice(INPUT_NEW_PASSWORD);	//语音提示请输入新密码
		pas_flag = 0;
	}
	if(key != 0xff)
	{
		if(key != '*' && key != '#')
		{
			if(pass_i <= 5)
			{
				sys_password[pass_i] = key;
				printf("*");
				pass_i ++;
				if(pass_i == 6)
				{
					printf("请再次输入你要修改的密码:\r\n");
					Voice(INPUT_NEW_PASSWORD_AGAIN);	//语音提示请再次输入新密码 
				}
			}
			else if(pass_i >= 6 && pass_i <= 11)
			{
				user_password[pass_i - 6] = key;
				printf("*");
				pass_i ++;
				if(pass_i == 12)
				{
					if(strcmp((char *)sys_password,(char *)user_password) == 0)	//密码一致
					{
						printf("修改密码成功:\r\n");
						Voice(SETTING_SUCCESS);	//语音提示操作成功
						AT24C04_write_bytes(11,sizeof(sys_password),sys_password,AT24C02);
						pass_i = 0;
						pas_flag = 1;
					}
					else	//密码不一致
					{
						printf("两次密码不一致:\r\n");
						Voice(PASSWORD_INCONFORMITY);	//语音提示密码不一致
						pass_i = 0;
						pas_flag = 1;
					}
				}
			}
		}
		else if(key == '*')
		{
			page_flag = 1;
			pas_flag = 1;
		}
		else if(key == '#')
		{
			page_flag = 2;
			pas_flag = 1;
		}
	}
}


/***********************************************
*函数名    :volume_control
*函数功能  :声音调节界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 7
************************************************/
void volume_control(u8 key)
{
	static u8 vol_flag = 1;
	if(vol_flag)
	{
		printf("音量调节界面：\r\n");
		printf("【1】音量:1\r\n");
		printf("【2】音量:2\r\n");
		printf("【3】音量:3\r\n");
		printf("【4】音量:4\r\n");
		printf("【#】回到上一级菜单\r\n");
		printf("【*】回到主界面\r\n");
		Voice(CHANGE_VOLUME);	//语音提示设置音量
		vol_flag = 0;
	}
	switch(key)
	{
		case '1':Voice(0x26);Tim5_Delay_Ms(10);printf("当前音量：1\r\n");Voice(SETTING_SUCCESS);break;	//语音提示操作成功
		case '2':Voice(0x24);Tim5_Delay_Ms(10);printf("当前音量：2：\r\n");Voice(SETTING_SUCCESS);break;
		case '3':Voice(0x22);Tim5_Delay_Ms(10);printf("当前音量：3：\r\n");Voice(SETTING_SUCCESS);break;
		case '4':Voice(0x20);Tim5_Delay_Ms(10);printf("当前音量：4：\r\n");Voice(SETTING_SUCCESS);break;
		case '#':page_flag=2;vol_flag=1;break;
		case '*':page_flag=1;vol_flag=1;break;
	}
}


/***********************************************
*函数名    :light_control
*函数功能  :屏幕亮度调节界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 8
************************************************/
void light_control(u8 key)
{
	static u8 light_flag = 1;
	if(light_flag)
	{
		printf("屏幕亮度调节界面：\r\n");
		printf("【1】亮度:1\r\n");
		printf("【2】亮度:2\r\n");
		printf("【3】亮度:3\r\n");
		printf("【4】亮度:4\r\n");
		printf("【5】亮度:5\r\n");
		printf("【#】回到上一级菜单\r\n");
		printf("【*】回到主界面\r\n");
		light_flag = 0;
	}
	switch(key)
	{
		case '1':printf("当前屏幕亮度：1\r\n");break;
		case '2':printf("当前屏幕亮度：2\r\n");break;
		case '3':printf("当前屏幕亮度：3\r\n");break;
		case '4':printf("当前屏幕亮度：4\r\n");break;
		case '5':printf("当前屏幕亮度：5\r\n");break;
		case '#':page_flag=2;light_flag=1;break;
		case '*':page_flag=1;light_flag=1;break;
	}
}
u8 card[9][4] = {{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},
								 {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},
								 {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},};

/***********************************************
*函数名    :card_page
*函数功能  :注册卡片函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 9
						卡号是数组下标+1
************************************************/
void card_page(u8 key)
{
	u8 i,j;
	u8 buff[20];
	static u8 temp = 0;
	u8 cont = 0;
	static u8 card_id[4];	//存卡号id
	u8 card_content[16];	//存卡片某数据块内容
	static u8 card_flag = 1;
	
	if(card_flag)
	{
		LCD_Clear(BLACK);
		card_flag = 0;
		AT24C04_read_bytes(31,36,(u8 *)card);
//		LCD_dis(0,0,YELLOW,"[#]回到上一级",32,0,0x0000);
//		LCD_dis(0,32,YELLOW,"[*]回到主界面",32,0,0x0000);
//		LCD_dis(0,64,YELLOW,"请放置卡片",32,0,0x0000);
//		
		for(i=0;i<9;i++)
		{
			for(j=0;j<4;j++)
			{
				if(card[i][j] == 0xff)
				{
					cont ++;		
				}
			}
			if(cont == 4)
			{
				temp = i;
//				sprintf(buff,"没注册过的卡号:%d",i+1);
//				LCD_dis(0,92,YELLOW,buff,32,0,0x0000);
				printf("没注册过的卡号:%d\r\n",i+1);	//没有注册过的卡号
				break;
			}
			cont = 0;
		}
		//循环出来i的值就是要存储的元素位置
		if(i==10)
		{
//			sprintf(buff,"注册卡片空间不足,请删除卡片再注册");
//			LCD_dis(0,92,YELLOW,buff,32,0,0x0000);
			printf("注册卡片空间不足,请删除卡片再注册\r\n");
			//将刚才写卡内容清空
			PcdReset();     //重新复位卡机为了解除卡休眠
			return ;
		}
	}
	
	if(PCD_distinguish_PICC(card_id,1) == MI_OK)	//识别卡片成功
	{
		for(i=0;i<9;i++)	//对比卡片id
		{
			for(j=0;j<4;j++)
			{
				if(card[i][j] != card_id[j])
				{
					break;
				}
			}
			if(j == 4)	//找到一致的卡片id
			{
//				sprintf(buff,"该卡片id已注册过，注册失败");
//				LCD_dis(0,160,YELLOW,buff,32,0,0x0000);
				printf("该卡片id已注册过，注册失败\r\n");
				card_flag = 1;
				return ;
			}
		}
		
		PcdReset();	//解除休眠
		for(j=0;j<4;j++)
		{
			card[temp][j] = card_id[j];	//将卡号id写进数组
		}
		AT24C04_write_bytes(31,36,(u8 *)(card),AT24C04);	//将卡片数组存进at24c04中
		/*卡片模块*/
		for(j=0;j<16;j++)	//将卡号id写进16个字节的数组
		{
			if(j<4)	card_content[j] =  card_id[j];	
			else card_content[j] =  0;	
		}
		WriteCardData(5,card_content);
//		sprintf(buff,"注册成功");
//		LCD_dis(0,160,YELLOW,buff,32,0,0x0000);
		printf("注册成功\r\n");
		Voice(SETTING_SUCCESS);	//语音提示操作成功
		card_flag = 1;
	}
	if(key == '*')
	{
		page_flag = 1;	//回到主界面
		card_flag = 1;	//下次进来显示
	}
	else if(key == '#')
	{
		page_flag = 10;
		card_flag = 1;	//下次进来显示
	}	
}


/***********************************************
*函数名    :card_match_1_n
*函数功能  :卡片开锁函数
*函数参数  :无
*函数返回值:无
*函数描述  :
						卡号是数组下标+1
************************************************/
void card_match_1_n(void)
{
	u8 i,j;
	u8 buff[20];
	u8 card_id[4];
	u8 card_content[16];
	static u8 card_open_flag = 1;
	if(card_open_flag)
	{
		LCD_Clear(BLACK);
		card_open_flag = 0;
//		LCD_dis(0,0,YELLOW,"请输入密码",32,0,0x0000);
//		LCD_dis(0,32,YELLOW,"请放下手指解锁",32,0,0x0000);
//		LCD_dis(0,64,YELLOW,"请放置卡片解锁",32,0,0x0000);
		printf("请放置卡片解锁\r\n");
		Voice(PUTCARD);	//语音提示请放置卡片
		AT24C04_read_bytes(31,36,(u8 *)card);
	}
	if(PCD_distinguish_PICC(card_id,1) == MI_OK)	//识别卡片成功
	{
		LCD_Clear(BLACK);
		for(i=0;i<9;i++)	//对比卡片id
		{
			for(j=0;j<4;j++)
			{
				if(card[i][j] != card_id[j])
				{
					break;
				}
			}
			if(j == 4)	//找到一致的卡片id
			{
//				sprintf(buff,"卡片id正确");
//				LCD_dis(0,32,YELLOW,buff,32,0,0x0000);
				printf("卡片id正确\r\n");
				PcdReset();	//解除休眠
				break;
			}
		}
		if(i == 9)
		{
//			sprintf(buff,"该卡片未注册，开门失败");
//			LCD_dis(0,64,YELLOW,buff,32,0,0x0000);
			printf("该卡片未注册，开门失败\r\n");
			Voice(DOOROPEN_FAIL);	//语音提示开门失败
			card_open_flag = 1;
			return ;
		}
		ReadCardData(5,card_content);	//读出卡片内容
		for(j=0;j<4;j++)
		{
			if(card[i][j] != card_content[j])	//对比卡片内容
			{
				break;
			}
		}
		if(j == 4)
		{
//			sprintf(buff,"卡片内容正确，开门成功");
//			LCD_dis(0,64,YELLOW,buff,32,0,0x0000);
			printf("卡片内容正确，开门成功\r\n");
			Voice(DOOROPEN_SUCCESS);	//语音提示欢迎回家
			Motor_Open();
			Tim5_Delay_Ms(1000);
			Motor_Close();
		}
		else
		{
//			sprintf(buff,"卡片内容错误，开门失败");
//			LCD_dis(0,64,YELLOW,buff,32,0,0x0000);
			printf("卡片内容错误，开门失败\r\n");
			Voice(DOOROPEN_FAIL);	//语音提示开门失败
		}
		card_open_flag = 1;
	}

}

/***********************************************
*函数名    :card_control_page
*函数功能  :射频卡管理界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 10
						卡号是数组下标+1
************************************************/
void card_control_page(u8 key)
{
	static u8 card_flag = 1;
	u8 new_card[9][4] = {{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},
											 {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},
											 {0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},{0xff,0xff,0xff,0xff},};
	if(card_flag)
	{
//		LCD_Clear(BLACK);
//		LCD_dis(0,0,YELLOW,"IC卡管理界面",32,0,0x0000);
//		LCD_dis(0,32,YELLOW,"[1]注册卡片",32,0,0x0000);
//		LCD_dis(0,64,YELLOW,"[2]删除one卡片",32,0,0x0000);
//		LCD_dis(0,96,YELLOW,"[3]删除all卡片",32,0,0x0000);
//		LCD_dis(0,128,YELLOW,"[#]回到上一级",32,0,0x0000);
//		LCD_dis(0,160,YELLOW,"[*]回到主界面",32,0,0x0000);
		printf("IC卡管理界面\r\n");
		printf("[1]注册卡片\r\n");
		printf("[2]删除one卡片\r\n");
		printf("[3]删除all卡片\r\n");
		printf("[#]回到上一级\r\n");
		printf("[*]回到主界面\r\n");
		card_flag = 0;
	}
	
	switch(key)
	{
		case '1':page_flag=9;card_flag = 1;break;
		case '2':page_flag=11;card_flag = 1;break;
		case '3':AT24C04_write_bytes(31,36,(u8 *)new_card,AT24C04);Voice(SETTING_SUCCESS);card_flag = 1;break;
		case '*':page_flag=1;card_flag = 1;break;
		case '#':page_flag=2;card_flag = 1;break;
	}
	
}

/***********************************************
*函数名    :card_one_page
*函数功能  :删除指定卡片界面函数
*函数参数  :u8 key
*函数返回值:无
*函数描述  :page_flag = 11
						卡号是数组下标+1
************************************************/
void card_one_page(u8 key)
{
	u8 i,j;
	u8 buff[20];
	u8 card_id[4];
	u8 cont = 0;	//记录已经注册的卡片
	static u8 card_flag = 1;
	
	if(card_flag)
	{
		LCD_Clear(BLACK);
//		LCD_dis(0,0,YELLOW,"删除指定卡片",32,0,0x0000);
//		LCD_dis(0,32,YELLOW,"[#]回到上一级",32,0,0x0000);
//		LCD_dis(0,64,YELLOW,"[*]回到主界面",32,0,0x0000);
		printf("删除指定卡片\r\n");
		printf("[#]回到上一级\r\n");
		printf("[*]回到主界面\r\n");
		Voice(DELETE_ASSIGN_CARD);	//语音提示删除指定门卡
		AT24C04_read_bytes(31,36,(u8 *)card);
		for(i = 0;i < 9;i ++)	//显示未注册的卡片
		{
			for(j = 0;j < 4;j ++)
			{
				if(card[i][j] != 0xff)
				{
					cont ++;
//					sprintf(buff,"已注册的id:%d",cont);
//					LCD_dis(0,96,YELLOW,buff,32,1,0x0000);
					printf("已注册的卡号id:%d\r\n",cont);
					break;
				}
			}
		}
//		sprintf(buff,"请选择ID号:");
//		LCD_dis(0,128,YELLOW,buff,32,1,0x0000);
		printf("请选择要删除的卡片ID号:\r\n");
		if(cont == 0)
		{
//			sprintf(buff,"未注册过卡片,请先注册卡片再删除");
//			LCD_dis(0,160,YELLOW,buff,32,0,0x0000);
			printf("未注册过卡片,请先注册卡片再删除\r\n");
			Voice(PASSWORD_ERROR);	//语音提示验证失败
			card_flag = 1;
			page_flag = 10;	//回到上一级菜单
			return ;
		}
		
		card_flag = 0;
	}

	if(key >= '1' && key <= '9')
	{
		for(u8 j=0;j<4;j++)
		{
			card[(key-48-1)][j] = 0xff;
		}
		AT24C04_write_bytes(31,36,(u8 *)card,AT24C04);
//		sprintf(buff,"删除成功");
//		LCD_dis(0,160,YELLOW,buff,32,0,0x0000);
		printf("删除成功\r\n");
		Voice(SETTING_SUCCESS);	//语音提示操作成功
		card_flag = 1;
	}
	else if(key == '*')
	{
		page_flag = 1;
		card_flag = 1;
	}
	else if(key == '#')
	{
		page_flag = 10;
		card_flag = 1;
	}
}






