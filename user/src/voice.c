#include "voice.h"
#include "time.h"

/********************************************************************
* 函数名 	：Voice_Init
* 函数功能	：语音播报初始化函数（语音播报模块所连接的GPIO口）
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  VOICE DATA(数据先)     ---PC5---通用推挽输出
*				VOICE BUSY(语音检测脚)  ---PC4---通用输入
*********************************************************************/
void Voice_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	//GPIO口配置
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;				//PC5（选择5号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;		//配置为输入模式
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;				//PC4（选择4号GPIO口）
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	//数据线输出低电平（数据线的空闲状态）
	VOICE_DATA_L;
	
}

/********************************************************************
* 函数名 	：Voice_Head
* 函数功能	：同步头
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  		8ms高电平 + 1ms低电平
*********************************************************************/
void Voice_Head(void)
{
	//输出高电平
	VOICE_DATA_H;
	//延时8ms
	Tim5_Delay_Ms(8);
	//输出低电平
	VOICE_DATA_L;
	//延时1ms
	Tim5_Delay_Ms(1);
	
}

/********************************************************************
* 函数名 	：Voice_Low
* 函数功能	：数据0函数
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  		0.5ms高电平 + 1.5ms低电平
*********************************************************************/
void Voice_Low(void)
{
	
	VOICE_DATA_H;						//输出高电平
	Tim5_Delay_Us(500);			//延时0.5ms
	VOICE_DATA_L;					//输出低电平	
	Tim5_Delay_Us(1500);	//延时1.5ms
}
	
/********************************************************************
* 函数名 	：Voice_High
* 函数功能	：数据1函数
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  		1.5ms高电平 + 0.5ms低电平
*********************************************************************/
void Voice_High(void)
{
	
	VOICE_DATA_H;						//输出高电平
	Tim5_Delay_Us(1500);		//延时1.5ms
	VOICE_DATA_L;						//输出低电平
	Tim5_Delay_Us(500);			//延时0.5ms
}

/********************************************************************
* 函数名 	：Voice
* 函数功能	：语音播报函数
* 函数参数	：u8 cmd   协议码
* 函数返回值	：void
* 函数说明	：
*			  		
*********************************************************************/
void Voice(u8 cmd)
{
	
	//等待上一段语音播放完成
	while(VOICE_BUSY);
	//把数据发送出去
	Voice_Head();
	
	for(u8 i=0;i<8;i++)
	{
		if(cmd &(0x80 >> i))
		{
			Voice_High();
		}
		else
		{
		  Voice_Low();
		}
	}
	
	
}





