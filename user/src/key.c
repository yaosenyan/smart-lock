#include "key.h"
#include "time.h"

/********************************************************************
* 函数名 	：Key_Init
* 函数功能	：key按键初始化函数（对按键所连接的GPIO口初始化配置）
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  KEY2 -- PC13 ---通用输入，不用上下拉
*********************************************************************/
void Key_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;		//配置为输入模式
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13;				//PB13（选择13号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_Init(GPIOC, &GPIO_InitStruct); 
	
}

/********************************************************************
* 函数名 	：Key_Scanf
* 函数功能	：按键扫描函数（判断按键是否按下）
* 函数参数	：void
* 函数返回值	：u8 = unsigned char  返回按键
* 函数说明	：
*			  机械按键有机械抖动 --- 消除抖动（延时消抖）
*				标志位：定义变量，去改变这个变量的值从而去控制程序执行
*********************************************************************/

u8 Key_Scanf(void)
{
		u8 keyval = 0xff;
	static u8 key_flag = 1;
	//判断按键是否按下
	if(!KEY2 && (key_flag == 1))								//如果按键按下
	{
		Tim5_Delay_Ms(15);//延时
		//再次判断按键是否按下
		if(!KEY2)
		{
			//把变量进行赋值
			keyval = 2;
			//锁死标志位
			key_flag = 0;
		}
	}
	//按键松开的时候去解锁标志位
	if(KEY2)
	{
		key_flag = 1;
	}
	return keyval;
}

