#include "lcd.h"
#include "time.h"
#include "font.h"
#include "string.h"
#include "lcd.h"
/********************************************************************
* 函数名 	：Spi_Init
* 函数功能	：spi初始化函数所用引脚
* 函数参数	：void
* 函数返回值	：void
* 函数说明	：
*			  				SCK --- PB13 --- 通用推挽输出，速度50MHz，无上下拉
*								SDA --- PB15 --- 通用推挽输出，速度50MHz，无上下拉
*********************************************************************/
	
void Spi_Init(void)
{	
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_15;				//PB13、15（选择13、15号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;	//配置50MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 

	//空闲状态
	LCD_SPI_SCL_L;			//时钟线低电平
	LCD_MOSI_H;					//数据线高电平
}


/********************************************************************
*	函数名：LCD_Spi_Byte
*	函数功能：SPI传输一个字节函数
*	函数参数：u8 data
*	函数返回值：void
*	函数描述：
*							时钟线为下降沿的时候发送数据
*********************************************************************/

void LCD_Spi_Byte(u8 data)
{
	
	
	for(u8 i=0;i<8;i++)
	{	
		LCD_SPI_SCL_L;
		if(data & 0x80)
		{
			LCD_MOSI_H;
		}
		else
		{
		  LCD_MOSI_L;
		}
		LCD_SPI_SCL_H;
		data <<= 1;
	}
		
}

/********************************************************************
*	函数名：LCD_Io_Init
*	函数功能：LCD的管脚初始化函数
*	函数参数：void
*	函数返回值：void
*	函数描述：
*							LEDK-----PB1    背光灯引脚   //通用推挽输出，速度2MHz，无上下拉
*							RESET----PB10   复位引脚     //通用推挽输出，速度2MHz，无上下拉
*							LCD_CS---PB12   片选灯引脚   //通用推挽输出，速度2MHz，无上下拉
*							LCD_D/C--PB14   数据命令选择引脚   //通用推挽输出，速度2MHz，无上下拉
*********************************************************************/
void LCD_Io_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1|GPIO_Pin_10|GPIO_Pin_12|GPIO_Pin_14;				//PB1、10、12、14（选择1、10、12、14号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 
	
	//关闭背光灯
	LCD_LED_OFF;
	//断开通讯
	LCD_CS_H;
	
}

/**************************************************
*函数名    ：st7789vm_write_command
*函数功能  ：对LCD写命令函数
*函数参数  ：u8 cmd
*函数返回值：void
*函数描述  ：    
****************************************************/
void st7789vm_write_command(u8 cmd)
{
	//打开通信
	LCD_CS_L;
	//选择发送命令
	LCD_CMD;
	//发送命令
	LCD_Spi_Byte(cmd);
	//关闭命令
	LCD_CS_H;
}

/**************************************************
*函数名    ：st7789vm_write_command_parameter
*函数功能  ：对LCD写命令参数值函数（发送数据函数）
*函数参数  ：u8 cmd_data		发送数据
*函数返回值：void
*函数描述  ：    
****************************************************/
void st7789vm_write_command_parameter(u8 cmd_data)
{
	//打开通信
	LCD_CS_L;
	//选择发送数据
	LCD_DATA;
	//发送数据
	LCD_Spi_Byte(cmd_data);
	//关闭通信
	LCD_CS_H;
}

/**************************************************
*函数名    ：st7789vm_write_data
*函数功能  ：对LCD写数据函数
*函数参数  ：u16 data
*函数返回值：void
*函数描述  ：坐标值，颜色值   
****************************************************/
void st7789vm_write_data(u16 data)
{
	//打开通信
	LCD_CS_L;
	//选择发送数据
	LCD_DATA;
	//发送数据
	LCD_Spi_Byte(data >> 8);//先发送高八位
	LCD_Spi_Byte(data);//再发送低八位
	//断开通信
	LCD_CS_H;
}

/**************************************************
*函数名    ：st7789vm_rest
*函数功能  ：对LCD复位函数
*函数参数  ：void
*函数返回值：void
*函数描述  ：  
*						复位：给复位引脚有一段时间的低电平
****************************************************/
void st7789vm_rest(void)
{
	//复位引脚输出低电平
	LCD_RES_L;
	//延时100ms
	Tim5_Delay_Ms(100);
	//复位引脚输出高电平
	LCD_RES_H;
}

/**************************************************
*函数名    ：LCD_init
*函数功能  ：对LCD初始函数
*函数参数  ：void
*函数返回值：void
*函数描述  ：  
****************************************************/
void LCD_Init(void)
{
	//调用spi引脚初始化
	Spi_Init();
	//调用lcd引脚初始化
	LCD_Io_Init();
	//调用屏幕复位函数
	st7789vm_rest();
	//屏幕驱动代码
	/* start initial sequence */ 
	st7789vm_write_command(0x36);
	st7789vm_write_command_parameter(0x00);

	st7789vm_write_command(0x3A); 
	st7789vm_write_command_parameter(0x05);

	st7789vm_write_command(0xB2);
	st7789vm_write_command_parameter(0x0C);
	st7789vm_write_command_parameter(0x0C);
	st7789vm_write_command_parameter(0x00);
	st7789vm_write_command_parameter(0x33);
	st7789vm_write_command_parameter(0x33); 

	st7789vm_write_command(0xB7); 
	st7789vm_write_command_parameter(0x35);  

	st7789vm_write_command(0xBB);
	st7789vm_write_command_parameter(0x19);

	st7789vm_write_command(0xC0);
	st7789vm_write_command_parameter(0x2C);

	st7789vm_write_command(0xC2);
	st7789vm_write_command_parameter(0x01);

	st7789vm_write_command(0xC3);
	st7789vm_write_command_parameter(0x12);   

	st7789vm_write_command(0xC4);
	st7789vm_write_command_parameter(0x20);  

	st7789vm_write_command(0xC6); 
	st7789vm_write_command_parameter(0x0F);    

	st7789vm_write_command(0xD0); 
	st7789vm_write_command_parameter(0xA4);
	st7789vm_write_command_parameter(0xA1);

	st7789vm_write_command(0xE0);
	st7789vm_write_command_parameter(0xD0);
	st7789vm_write_command_parameter(0x04);
	st7789vm_write_command_parameter(0x0D);
	st7789vm_write_command_parameter(0x11);
	st7789vm_write_command_parameter(0x13);
	st7789vm_write_command_parameter(0x2B);
	st7789vm_write_command_parameter(0x3F);
	st7789vm_write_command_parameter(0x54);
	st7789vm_write_command_parameter(0x4C);
	st7789vm_write_command_parameter(0x18);
	st7789vm_write_command_parameter(0x0D);
	st7789vm_write_command_parameter(0x0B);
	st7789vm_write_command_parameter(0x1F);
	st7789vm_write_command_parameter(0x23);

	st7789vm_write_command(0xE1);
	st7789vm_write_command_parameter(0xD0);
	st7789vm_write_command_parameter(0x04);
	st7789vm_write_command_parameter(0x0C);
	st7789vm_write_command_parameter(0x11);
	st7789vm_write_command_parameter(0x13);
	st7789vm_write_command_parameter(0x2C);
	st7789vm_write_command_parameter(0x3F);
	st7789vm_write_command_parameter(0x44);
	st7789vm_write_command_parameter(0x51);
	st7789vm_write_command_parameter(0x2F);
	st7789vm_write_command_parameter(0x1F);
	st7789vm_write_command_parameter(0x1F);
	st7789vm_write_command_parameter(0x20);
	st7789vm_write_command_parameter(0x23);

	st7789vm_write_command(0x21); 
	st7789vm_write_command(0x11); 
	st7789vm_write_command(0x29); 
	
	//打开背光灯
	LCD_LED_ON;
	
	//调用清屏函数
	LCD_Clear(BLACK);
}

/**************************************************
*函数名    ：LCD_clear
*函数功能  ：清屏
*函数参数  ：u16 color
*函数返回值：无
*函数描述  ：  
****************************************************/
void LCD_Clear(u16 color)
{
	//确定横坐标
		st7789vm_write_command(0x2a);
		st7789vm_write_data(0);//确定横坐标的起始坐标
		st7789vm_write_data(LCD_W - 1);//确定横坐标的结束坐标

	//确定纵坐标
	  st7789vm_write_command(0x2b);
		st7789vm_write_data(0);//确定纵坐标的起始坐标
		st7789vm_write_data(LCD_H - 1);//确定纵坐标的结束坐标
	
	//发送颜色命令
	st7789vm_write_command(0x2c);
	//发送颜色数据
	for(u32 i = 0;i<LCD_W * LCD_H;i++)
	{
		st7789vm_write_data(color);
	}
	
}
/**************************************************
*函数名    ：LCD_Point
*函数功能  ：打点函数
*函数参数  ：u8 x,u8 y，u16 color
*函数返回值：void
*函数描述  ：  
****************************************************/
void LCD_Point(u8 x,u8 y,u16 color)
{
	//确定横向坐标
		st7789vm_write_command(0x2a);
		st7789vm_write_data(x);//确定横坐标的起始坐标
		st7789vm_write_data(x);//确定横坐标的结束坐标
	
	//确定纵向坐标
		st7789vm_write_command(0x2b);
		st7789vm_write_data(y);//确定纵坐标的起始坐标
		st7789vm_write_data(y);//确定纵坐标的结束坐标
	
	st7789vm_write_command(0x2c);//发送显示颜色命令
	
	//发送颜色出去

		st7789vm_write_data(color);
	
}




/**************************************************
*函数名    ：LCD_dis_onechr
*函数功能  ：显示一个字符函数
*函数参数  ：u8 x,u8 y
*函数返回值：void
*函数描述  ：  
****************************************************/
u8 color_buff[] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x38,0x44,0x0C,0x34,0x44,0x4C,0x36,0x00,0x00};
void LCD_dis_onechr(u8 x,u8 y)
{
	for(u8 i=0;i<16;i++)					//纵向
	{
		for(u8 j=0;j<8;j++)					//横向
		{
			if(color_buff[i] & 0x80 >> j)
			{
				LCD_Point(x+j,y+i,RED);
			}
			else
			{
				LCD_Point(x+j,y+i,WHITE);
			}
		}
	}
	
}

/**************************************************
*函数名    ：LCD_dis_ch
*函数功能  ：显示一个字符函数
*函数参数  ：u8 x,u8 y,u8 ch,u8 size,u16 color,u8 mode,u16 mode_color
*函数返回值：void
*函数描述  ：  
****************************************************/
void LCD_dis_ch(u8 x,u8 y,u8 ch,u8 size,u16 color,u8 mode,u16 mode_color)
{
	u8 n,i,j;
	u8 temp;
	u16 temp32;
	
	//计算偏移值
  n = ch - ' ';
	
	//如果字符是16大小
	if(size == 16)
	{
		for(i=0;i<size;i++)
		{
			//用变量接收取模数据
			temp = ASC16[n*16 + i];
			for(j=0;j<size/2;j++)
			{
				if(temp & 0x80>>j)			//如果模数据是1
				{
					LCD_Point(x+j,y+i,color);		//打点
				}
				else
				{
					if(mode == 1)				//是否需要背景颜色
					{
						LCD_Point(x+j,y+i,mode_color);	//打上背景颜色
					}
				}
			}
		}
	}
	//如果字符是32大小
	else if(size == 32)
	{
		for(i=0;i<size;i++)
		{
			//用变量接收取模数据
			temp32 = (ASC32[n*64 + i*2]<<8) | (ASC32[n*64 + i*2+1]);
			for(j=0;j<size/2;j++)
			{
				if(temp32 & 0x8000>>j)			//如果模数据是1 
				{
					LCD_Point(x+j,y+i,color);		//打点
				}
				else
				{
					if(mode == 1)				//是否需要背景颜色
					{
						LCD_Point(x+j,y+i,mode_color);	//打上背景颜色
					}
				}
			}
		}
	}
}

/**************************************************
*函数名    ：LCD_dis_onehz
*函数功能  ：显示一个汉字函数
*函数参数  ：u8 x,u8 y
*函数返回值：void
*函数描述  ：  
****************************************************/
u8 hz_buff[] = {
0x08,0x40,0x08,0x20,0x0B,0xFE,0x10,0x00,0x10,0x00,0x31,0xFC,0x30,0x00,0x50,0x00,
0x91,0xFC,0x10,0x00,0x10,0x00,0x11,0xFC,0x11,0x04,0x11,0x04,0x11,0xFC,0x11,0x04,/*"信",0*/
};
void LCD_dis_onehz(u8 x,u8 y)
{
	//定义变量
	u16 temp;
		for(u8 i=0;i<16;i++)					//纵向
	{
		//用变量去获取数据
		temp = (hz_buff[i*2] << 8 | hz_buff[i*2+1]);	
		for(u8 j=0;j<16;j++)					//横向
		{

			if(temp & 0x8000 >> j)
			{
				LCD_Point(x+j,y+i,RED);
			}
			else
			{
				LCD_Point(x+j,y+i,WHITE);
			}
		}
	}
}

/******************************************************************************
*函数名    ：LCD_dis_hz
*函数功能  ：LCD屏幕显示可选大小的汉字
*函数参数  ：u16 x,u16 y,u16 color，u8 *hz，u8 size ,u8 mode,u16 mode_color 
*函数返回值：无
*函数描述  ：
						hz:"好"
						size 字号大小   16  32
						mode 传入1  就带背景颜色
						mode 传入0  就不带背景颜色
********************************************************************************/
void LCD_dis_hz(u16 x,u16 y,u16 color,u8 *hz,u8 size,u8 mode,u16 mode_color)
{
	//定义变量
	u8 i,j,n=0;
	u16 temp16;
	u32 temp32;
	//计算偏移量
	while(table[2*n] != '\0')
	{
		if(*hz == table[2*n] && *(hz+1) == table[2*n+1])
		{
			break;
		}
		n++;
	}
	//如果没有显示模数据就结束函数
	if(n == (strlen((char *)table)/2))
	{
		return;
	}
	//字体16*16
	if(size == 16)
	{
		for(i=0;i<size;i++)
		{
			//用变量去接收数据
			temp16 = ((hz16[32*n+2*i])<<8 | (hz16[32*n+2*i+1]));
			
			for(j=0;j<size;j++)
			{
				if(temp16 & 0x8000>>j)//如果模数据对应位为1
				{
					LCD_Point(x+j,y+i,color);
				}
				else
				{
					if(mode == 1)//是否需要打背景颜色
					{
						LCD_Point(x+j,y+i,mode_color);
					}
				}
			}
		}
	}
	// 字体32*32
	else if(size == 32)
	{
		for(i=0;i<size;i++)
		{
			//用变量去接收数据
			temp32 = (hz32[128*n+4*i]<<24)	|
						   (hz32[128*n+4*i+1]<<16)|
							 (hz32[n*128+4*i+2]<<8) |
							  hz32[n*128+4*i+3];
			
			for(j=0;j<size;j++)
			{
				if(temp32 & 0x80000000>>j)//如果模数据对应位为1
				{
					LCD_Point(x+j,y+i,color);
				}
				else
				{
					if(mode == 1)//是否需要打背景颜色
					{
						LCD_Point(x+j,y+i,mode_color);
					}
				}
			}
		}
	}
	
}

/******************************************************************************
*函数名    ：LCD_dis
*函数功能  ：LCD屏幕显示可选大小的汉字和字符混合
*函数参数  ：u16 x,u16 y,u16 color，u8 *str，u8 size ,u8 mode,u16 mode_color 
*函数返回值：无
*函数描述  ：
						size 字号大小   16  32
						mode 传入1  就带背景颜色
						mode 传入0  就不带背景颜色
********************************************************************************/
void LCD_dis(u16 x,u16 y,u16 color,u8 *str,u8 size ,u8 mode,u16 mode_color )
{
	while(*str != '\0')
	{
		//如果是字符
		if((*str >=32) && (*str <=127))
		{
			LCD_dis_ch(x,y,*str,size,color,mode,mode_color);
			str++;
			x+=size/2;
			if(x>239-size)
			{
				y+=size;
				x=0;
			}
		}
		//汉字
		else
		{
			LCD_dis_hz(x,y,color,str,size,mode,mode_color);
			str+=2;
			x+=size;
			if(x>239-size)
			{
				y+=size;
				x=0;
			}
		}
	}
	
}

/******************************************************************************
*函数名    ：LCD_dis_pic
*函数功能  ：LCD屏幕显示任意大小图片
*函数参数  ：u16 x,u16 y,u8 *pic 
*函数返回值：void
*函数描述  ：						
********************************************************************************/
void LCD_dis_pic(u16 x,u16 y,u8 *pic)
{
	//定义变量
	HEADCOLOR *head;
	u16 *p;
	u32 i;
	
	//提取图像的头数据
	head = (HEADCOLOR*)pic;
	
	//用指针获取图像的颜色数据
	p = (u16 *)(pic+sizeof(HEADCOLOR));  //pic[sizeof(HEADCOLOR)]
	
	//确定显示位置
	//横向方向
	st7789vm_write_command(0x2a);
	st7789vm_write_data(x);
	st7789vm_write_data(x+(head->w)-1);
	
	//纵向方向
	st7789vm_write_command(0x2b);
	st7789vm_write_data(y);
	st7789vm_write_data(y+(head->h)-1);
	
	
	//发送颜色命令
	st7789vm_write_command(0x2c);
	//发送颜色数据
	for(i=0;i<(head->h)*(head->w);i++)
	{
		st7789vm_write_data(*p);
		p++;
	}
	
	
}










