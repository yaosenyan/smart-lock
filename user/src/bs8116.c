#include "main.h"

/***********************************************
*函数名    :bs8116_iic_IO_init
*函数功能  :IIC所用IO口初始化
*函数参数  :无
*函数返回值:无
*函数描述  :IIC_SCL----------PB6------通用推挽输出
            IIC_SDA----------PB7------通用开漏输出
************************************************/
void bs8116_iic_IO_init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;		//配置输出开漏
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;				//PB6（选择6号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 
	
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;		//配置输出开漏
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;				//PB7（选择7号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 
	
	//输出高电平
	GPIO_SetBits(GPIOB, GPIO_Pin_6);
	GPIO_SetBits(GPIOB, GPIO_Pin_7);
	
}

/***********************************************
*函数名    :bs8116_iic_star
*函数功能  :IIC的起始信号函数
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void bs8116_iic_star(void)
{
	//拉低时钟线
	BS8116_IIC_SCL_L;
	
	//延时5us
	Tim5_Delay_Us(5);
	
	//拉高数据线
	BS8116_IIC_SDA_H;
	
	//时钟线高电平
	BS8116_IIC_SCL_H;
	
	//延时5us
	Tim5_Delay_Us(5);		
	
	//拉低数据线产生下降沿
	BS8116_IIC_SDA_L;
	
	//延时5us
	Tim5_Delay_Us(5);
	
	//安全作用
	BS8116_IIC_SCL_L;	
}

/***********************************************
*函数名    :bs8116_iic_stop
*函数功能  :IIC的停止信号函数
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void bs8116_iic_stop(void)
{
	//拉低时钟线
	BS8116_IIC_SCL_L;
	
	//延时5us
	Tim5_Delay_Us(5);
	
	//拉低数据线
	BS8116_IIC_SDA_L;
	
	//时钟线高电平
	BS8116_IIC_SCL_H;
	
	//延时5us
	Tim5_Delay_Us(5);		
	
	//拉高数据线产生上升沿
	BS8116_IIC_SDA_H;
	
	//延时5us
	Tim5_Delay_Us(5);
	
	//安全作用
	BS8116_IIC_SCL_L;
	
}

/**********************************************************
*函数名    :bs8116_iic_send_ack
*函数功能  :IIC发送应答/不应答函数
*函数参数  :u8 ack
*函数返回值:无
*函数描述  :ack:  0   应答      告诉从机，主机还要接收数据
           ack:  1   不应答     告诉从机，主机不要接收数据
***********************************************************/
void bs8116_iic_send_ack(u8 ack)
{
	//拉低时钟线
	BS8116_IIC_SCL_L;
	//延时
	Tim5_Delay_Us(5);
	
	//判断参数
	if(ack == 0)
	{
		//拉低数据线产生应答信号
		BS8116_IIC_SDA_L;
	}
	else if(ack == 1)
	{
		//拉高数据线产生不应答信号
		BS8116_IIC_SDA_H;
	}
	//延时
	Tim5_Delay_Us(5);
	
	//拉高时钟线
	BS8116_IIC_SCL_H;
	
	//延时
	Tim5_Delay_Us(5);
	
	//安全作用
	BS8116_IIC_SCL_L;
		
}

/***********************************************
*函数名    :bs8116_iic_get_ack
*函数功能  :IIC接收应答/不应答函数
*函数参数  :无
*函数返回值:u8
*函数描述  :返回0   应答         从机接收数据成功
           返回1   不应答        从机接收数据失败
************************************************/
u8 bs8116_iic_get_ack (void)
{
	u8 ack;
	//把数据线切换为输入模式
	BS8116_IIC_SCL_L;
	BS8116_IIC_SDA_H;  //输出堵死
	
	//检测
	BS8116_IIC_SCL_L;
	//延时
	Tim5_Delay_Us(5);
	
	BS8116_IIC_SCL_H;
	//延时
	Tim5_Delay_Us(5);
	
	//判断数据线如果为高电平
	if(BS8116_IIC_SDA_IN)
	{
		ack = 1;//把变量赋值为1
	}
	else
	{
		ack = 0;//把变量赋值为0
	}
	return ack;//返回变量
	
}

/***********************************************
*函数名    :bs8116_iic_send_byte
*函数功能  :IIC发送一个字节函数
*函数参数  :u8
*函数返回值:无
*函数描述  :
************************************************/
/***********************************************
*函数名    :bs8116_iic_send_byte
*函数功能  :IIC发送一个字节函数
*函数参数  :u8
*函数返回值:无
*函数描述  :
************************************************/
void bs8116_iic_send_byte(u8 data)
{
	u8 i;
	for(i=0;i<8;i++)     //循8次
	{
		BS8116_IIC_SCL_L;         //为了写入数据
		//延时5us
	  Tim5_Delay_Us(5);
		if(data & 0x80)    //判断最高位是什么  
		{
		  BS8116_IIC_SDA_H;
		}
		else
		{
			BS8116_IIC_SDA_L;
		}
		//延时5us
	  Tim5_Delay_Us(5);
		BS8116_IIC_SCL_H;          //帮助从机读取数据线的数据
		//延时5us
	  Tim5_Delay_Us(5);
		data = data<<1;     //把此高位变成最高位
	}
	
	//安全作用
	BS8116_IIC_SCL_L;
}

/***********************************************
*函数名    :bs8116_iic_rec_byte
*函数功能  :IIC接收一个字节函数
*函数参数  :无
*函数返回值:u8 
*函数描述  :
************************************************/
u8 bs8116_iic_get_byte(void)
{
	u8 i;
	u8 data;
	/*将输出线切换为输入模式*/
	BS8116_IIC_SCL_L;
  BS8116_IIC_SDA_H;
	
	
	/*接收一个字节数据*/
	//帮助从机发送数据
	for(i=0;i<8;i++)
	{
		BS8116_IIC_SCL_L;
		//延时5us
	  Tim5_Delay_Us(5);
	 	BS8116_IIC_SCL_H;
		//延时5us
  	Tim5_Delay_Us(5);
		data = data << 1;                 
		if(BS8116_IIC_SDA_IN)
		{
			data = data | 0x01;             //给最低位赋值1
		}
  }
	//安全
	BS8116_IIC_SCL_L;

	return data;
}


/***********************************************
*函数名    :BS8116_Readregister
*函数功能  :BS8116读寄存器的值
*函数参数  :u8 *buff 
*函数返回值:u8 
*函数描述  :读到的21个寄存器的值
************************************************/
u8 BS8116_Readregister(u8 *buff)
{

	bs8116_iic_star();
	bs8116_iic_send_byte(BS8116_W);  
	if(bs8116_iic_get_ack())
	{
		bs8116_iic_stop( );
		return 1;
	}
	
	//从机把时钟拉低表示正在处理数据
	//主机需要等待从机把时钟线拉高
	while(!BS8116_IIC_SCL_IN);
	
	
	bs8116_iic_send_byte(0xB0);     //从0xb0开始读
	if(bs8116_iic_get_ack())
	{
		bs8116_iic_stop( );
		return 2;
	}
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);
	
	bs8116_iic_star();
	bs8116_iic_send_byte(BS8116_R);  
	if(bs8116_iic_get_ack())
	{
		bs8116_iic_stop( );
		return 3;
	}
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);
	
	//读21个配置寄存器数据
	for(u8 i=1;i<22;i++)      
	{
		*buff = bs8116_iic_get_byte();
		if(i==21) bs8116_iic_send_ack(1);        //发送不应答信号
		else     bs8116_iic_send_ack(0);         //发送应答信号
		buff++;
	}
	bs8116_iic_stop( );
	
	return 0;
}

/***********************************************
*函数名    :BS8116_Writeregister
*函数功能  :BS8116写寄存器的值
*函数参数  :u8 *cmd_buff
*函数返回值:u8      //ack
*函数描述  :
************************************************/
u8 BS8116_Writeregister(u8 *cmd_buff)
{
	u32 check_sum = 0;
	
	bs8116_iic_star();                          //起始信号函数
	bs8116_iic_send_byte(BS8116_W);         //发送器件+写控制位
	if(bs8116_iic_get_ack())                    //检测应答
	{
		bs8116_iic_stop( );                       //停止信号函数
		return 1;                                //停止信号函数
	}
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);
	
	bs8116_iic_send_byte(0xB0);                 //寄存器的起始地址 
	if(bs8116_iic_get_ack())                    //检测应答
	{
		bs8116_iic_stop( );                       //停止信号函数
		return 2;                                 //停止信号函数
	}
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);
	
	/*发送数据*/
	//发送21个寄存器数据和一个累加和数据
	for(u8 i=1;i<=22;i++)
	{
		//第22次发送累加和
		if(i==22)
		{
			bs8116_iic_send_byte(check_sum & 0xff);     //发送校验和 低八位      
		}
		//21之前要发送寄存器数据并且累加
		else
		{
			bs8116_iic_send_byte(*cmd_buff);
			check_sum = check_sum + *cmd_buff;
			cmd_buff++;
		}
	
		//主机检测应答
		if(bs8116_iic_get_ack())
		{
			bs8116_iic_stop( );
			return 3;
		}
		
		//等待从机不忙
		while(!BS8116_IIC_SCL_IN);
	}
	
	bs8116_iic_stop( );
	
	return 0;
}



/***********************************************
*函数名    :BS8116_init
*函数功能  :BS8116初始化配置
*函数参数  :无
*函数返回值:无
*函数描述  :IRQ-----PA1
************************************************/
void BS8116_init(void)
{
	//定义变量
	u8 i;
	u8 w_buff[21] = {0x00,0x00,0x83,0xf3,0x98,
											0x3f,0x3f,0x3f,0x3f,0x3f,
											0x3f,0x3f,0x3f,0x3f,0x3f,
											0x3f,0x3f,0x3f,0x3f,0x3f,0x50};
	u8 r_buff[21];
	//调用IICio口初始化										
	bs8116_iic_IO_init();
											
	//IRQ初始化
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;		//配置为输入模式
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;				//PB1（选择1号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_Init(GPIOA, &GPIO_InitStruct); 										
	
	//读出设置寄存器原来的值
	BS8116_Readregister(r_buff);
	for(i=0;i<21;i++)
	{
		printf("%x:0x%x\r\n",0xb0+i,r_buff[i]);
	}		
											
	//往设置寄存器写入设置
	BS8116_Writeregister(w_buff);
		
	//读出设置寄存器设置好的值
	BS8116_Readregister(r_buff);
	for(i=0;i<21;i++)
	{
		printf("%x:0x%x\r\n",0xb0+i,r_buff[i]);
	}
	
}

/***********************************************
*函数名    :BS8116_ReadKey
*函数功能  :BS8116读键值
*函数参数  :无
*函数返回值:u8
*函数描述  :
************************************************/
u8 BS8116_ReadKey(void)
{
	u16 key = 0xff;
	
	
	bs8116_iic_star();
	bs8116_iic_send_byte(BS8116_W);  
	if(bs8116_iic_get_ack())
	{
		bs8116_iic_stop( );
		return 1;
	}
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);

	bs8116_iic_send_byte(0x08);    //发送键值寄存器的地址
	if(bs8116_iic_get_ack())
	{
		bs8116_iic_stop( );
		return 2;
	}
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);
	
	
	bs8116_iic_star();
	bs8116_iic_send_byte(BS8116_R);
	if(bs8116_iic_get_ack())
	{
		bs8116_iic_stop( );
		return 3;
	}  
	//等待从机不忙
	while(!BS8116_IIC_SCL_IN);
	
	key=bs8116_iic_get_byte();   //08
	bs8116_iic_send_ack(0);        //继续读
	key = (bs8116_iic_get_byte()<<8) | key;  
	bs8116_iic_send_ack(1);        //不需要数据了
	bs8116_iic_stop( );
	//key值就是键值寄存器的总体数据
	
//	printf("key:0x%x\r\n",key);

	
	
	switch(key)
	{
		case 0X0001:return   '1';break;
		case 0X0400:return   '2';break;
		case 0X0080:return   '3';break;     
		case 0X0002:return   '4';break;
		case 0x0800:return   '5';break;
		case 0X0040:return   '6';break;
		case 0X0008:return   '7';break;
		case 0X0100:return   '8';break;
		case 0X0020:return   '9';break;
		case 0X0004:return   '*';break;
		case 0X0200:return   '0';break;
		case 0X0010:return   '#';break;
	}	

	return 0xff;
}

/***********************************************
*函数名    :BS8116_Key_scan
*函数功能  :触摸按键扫描函数
*函数参数  :无
*函数返回值:u8
*函数描述  :
************************************************/
u8 BS8116_Key_scan(void)
{
	//定义变量
	u8 key = 0xff;
	static u8 bs8116_flag = 1;
	//按键按下
	if(!BS8116_IRQ && bs8116_flag)
	{
		//调用读键值，用变量获取返回值
		key = BS8116_ReadKey();
		//锁死标志位
		bs8116_flag = 0;
		//语音播报
		Voice(0x1d);
	}
	
	//按键松开
	if(BS8116_IRQ)
	{
		//解锁标志位
		bs8116_flag = 1;
	}
	return key;
}

/***********************************************
*函数名    :open_passward
*函数功能  :密码开锁
*函数参数  :u8 bs8116_key
*函数返回值:u8
*函数描述  :
************************************************/
void open_passward(u8 bs8116_key)
{
	static u8 at24_flag = 1;
	static u8 pss_open_flag;
	static u8 passward_i = 0;
	static u8 open_passward1[7] = {0};
	static u8 open_passward2[7] = {0};
	static u8 open_door_flag = 1;
	
	
	/*读取开机标志位*/   
	//读AT24C04 10号地址 是否为 0xff
	if(at24_flag == 1)
	{
		at24_flag = 0;                             //锁死标志位
		AT24C04_read_byte(20,&pss_open_flag);
		if(pss_open_flag != 0xfe)   Voice(0x02);   //语音提示设置密码
		LCD_Clear(BLACK);
	}

	
	/*第一次开机就设置密码*/
	if(pss_open_flag != 0xfe)
	{
		LCD_dis(0,40,YELLOW,(u8 *)"请设置开锁密码",32,0,0x0000);
		LCD_dis(0,75,YELLOW,(u8 *)"输入第一次密码",32,0,0x0000);
		//有效按键进入密码输入
		if(bs8116_key != 0xff)
		{
			//第一次输入密码   //6位
			if(passward_i<=5 )
			{	
				open_passward1[passward_i] = bs8116_key;
				LCD_dis(passward_i*32,110,YELLOW,(u8 *)"*",32,0,0x0000);
				passward_i++;
				if(passward_i == 6)
				{
					LCD_dis(0,145,YELLOW,(u8 *)"输入第二次密码",32,0,0x0000);
					Voice(0x0e);         //请再次输入新密码 语音提示
				}
			}
      //第二次输入密码
			else if(passward_i>=6 && passward_i<=11)
			{
				open_passward2[passward_i-6] = bs8116_key;
				LCD_dis((passward_i-6)*32,180,YELLOW,(u8 *)"*",32,0,0x0000);
				passward_i++;
				
				//两次密码输入完成进行对比
				if(passward_i == 12)
				{
					if(strcmp((char *)open_passward1,(char *)open_passward2) == 0)    //strcmp
					{
						//把密码存储到AT24C04
						AT24C04_write_bytes(11,sizeof(open_passward1),open_passward1,AT24C02);
						//开机标志位
						pss_open_flag = 0xfe;
						AT24C04_write_byte(20,pss_open_flag);
						LCD_Clear(0x0000);
						LCD_dis(0,0,YELLOW,(u8 *)"密码设置成功",32,0,0x0000);
						Voice(0x11);                   //操作成功语音提示
						passward_i = 0;
					}
					else
					{
						LCD_Clear(0x0000);
						LCD_dis(0,0,YELLOW,(u8 *)"两次密码不一致",32,0,0x0000);
						Voice(0x10);                  //密码不一致语音提示
						passward_i = 0;
						at24_flag = 1;
					}
				}
			}
			
		}		
	}

	
	
	 /*不是第一次开机  识别密码开门*/
	else
	{
		//开锁提示语
		if(open_door_flag)
		{
			open_door_flag = 0;
			LCD_Clear(0x0000);
			LCD_dis(0,0,YELLOW,(u8 *)"请输入密码",32,0,0x0000);
			Voice(0X0c);                //请输入管理员密码语音提示
		}
		//获取键盘密码对比
		if(passward_i<=5 && bs8116_key !=0xff)
		{	
			open_passward1[passward_i] = bs8116_key;
			LCD_dis(passward_i*32,110,YELLOW,(u8 *)"*",32,0,0x0000);
			passward_i++;
			if(passward_i == 6)
			{
				//读出AT24C02原始密码
				AT24C04_read_bytes(11,sizeof(open_passward1),open_passward2);
				//对比密码正确
				if(strcmp((char *)open_passward1,(char *)open_passward2) == 0)
				{
					LCD_Clear(0x0000);
					LCD_dis(50,100,YELLOW,(u8 *)"欢迎回家",32,0,0x0000);
					Voice(0X18);        //欢迎回家语音提示
					//开门
					Motor_Open();
					Tim5_Delay_Ms(300);
					Motor_Close();
					LCD_Clear(0x0000);
					passward_i = 0;
					open_door_flag = 1;
					page_flag = 2;
				}
				
				//对比密码错误
				else
				{
					LCD_Clear(0x0000);	
					LCD_dis(50,100,YELLOW,(u8 *)"密码错误",32,0,0x0000);
					Voice(0X19);       //开门失败语音提示
					passward_i = 0;
					open_door_flag = 1;	
			 }	
		 	}
		 }
	 }
}




